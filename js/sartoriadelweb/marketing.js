$(document).ready(function() {

  $('div[data-js="image-loader"] img').imageloader( { callback: function (elm) { $(elm).css("visibility","visible") } });

  $('.image-with-text > .big-text').fitText(1, { minFontSize: '50px', maxFontSize: '80px' });
  
});